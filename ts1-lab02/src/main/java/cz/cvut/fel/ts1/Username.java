package cz.cvut.fel.ts1;

public class Username {

    public long factorial (int n){
        //CONDITIONS
        if(n<0){
            System.out.println("You must enter number above or equal to zero.");
            return -1;
        }
        if(n==0)
            return 1;
        //FACTORIAL - iterativly
        long ret = 1;
        for(int i=2;i<=n;i++)
            ret = ret*i;
        return ret;
    }
}
