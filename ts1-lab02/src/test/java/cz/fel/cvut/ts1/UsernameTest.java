package cz.fel.cvut.ts1;
import cz.cvut.fel.ts1.Username;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UsernameTest {
    @Test
    public void factorialTest(){
        Username test = new Username();
        //TEST 1 - number below zero
        System.out.println("TEST 1: below zero");
        long test1 = test.factorial(-1);
        //TEST 2 - zero
        long test2 = test.factorial(0);
        System.out.println("TEST 2: equal to zero");
        System.out.println(test2);
        //TEST3 - number greater than zerpo
        long test3 = test.factorial(5);
        System.out.println("TEST 3 - greater than zero:");
        System.out.println(test3);
    }
}
