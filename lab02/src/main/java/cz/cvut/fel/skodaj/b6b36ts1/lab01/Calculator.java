/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.skodaj.b6b36ts1.lab01;

/**
 * Class representing some basic calculations
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Calculator
{
    
    /**
     * Computes factorial of given number
     * @param n Number from which will be computed factorial
     * @return Computed factorial
     */
    long Factorial(int n) 
    { 
        long res = 1, i; 
        for (i=2; i<=n; i++) 
            res *= i; 
        return res; 
    } 
}
