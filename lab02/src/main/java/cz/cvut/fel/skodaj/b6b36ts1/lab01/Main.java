/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.skodaj.b6b36ts1.lab01;

/**
 * Main class of whole project
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class Main
{
    public static void main(String[] args)
    {
        Calculator c = new Calculator();
        System.out.println(c.Factorial(10));
    }
    
}
