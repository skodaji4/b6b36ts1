/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.skodaj.b6b36ts1.lab01;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * JUnit tests for class Calculator
 * @author Jiri Skoda <skodaji4@fel.cvut.cz>
 */
public class CalculatorTest
{
    
    @Test
    public void Factorial_n1_res1()
    {
        Calculator instance = new Calculator();
        long result = instance.Factorial(1);
        long expected = 1;
        assertEquals(expected, result);
    }
    
    @Test
    public void Factorial_n2_res2()
    {
        Calculator instance = new Calculator();
        long result = instance.Factorial(2);
        long expected = 2;
        assertEquals(expected, result);
    }
    
    
    @Test
    public void Factorial_n3_res6()
    {
        Calculator instance = new Calculator();
        long result = instance.Factorial(3);
        long expected = 6;
        assertEquals(expected, result);
    }
    
    @Test
    public void Factorial_n4_res24()
    {
        Calculator instance = new Calculator();
        long result = instance.Factorial(4);
        long expected = 24;
        assertEquals(expected, result);
    }
    
    @Test
    public void Factorial_n5_res120()
    {
        Calculator instance = new Calculator();
        long result = instance.Factorial(5);
        long expected = 120;
        assertEquals(expected, result);
    }
    
}
